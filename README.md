[![unlicense](https://img.shields.io/badge/un-license-green.svg?style=flat)](https://unlicense.org)

# FerriSpark

A USB-C powered STM32L1 dev board designed to fit the 18×29 mm footprint of a well known micro-USB ATTiny85 board, made in [Horizon EDA](https://horizon-eda.org/).
The name is a reference to the aforementioned board as well as [Rust](https://github.com/stm32-rs/stm32l1xx-hal/pull/14).

![Photo](https://dl.unrelenting.technology/ferrispark-r0.jpg)

## License

This is free and unencumbered ~~software~~ hardware released into the public domain.  
For more information, please refer to the `UNLICENSE` file or [unlicense.org](https://unlicense.org).

This does not apply to the cached pool contents, see [the horizon-pool license](https://github.com/horizon-eda/horizon-pool/blob/master/LICENSE.md).
(And even this does not apply to the USB-C connector model which is like under GrabCAD non-commercial terms only)
